﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WebNuocHoa.Models;

namespace WebNuocHoa.Controllers
{
    public class HomeController : Controller
    {
        WebDB db = new WebDB();
        public ActionResult Index(int? id)
        {
            var Products = new List<product>();
            if (id == null)
            {
                Products = db.products.Select(h => h).ToList();
            }
            else
            {
                Products = db.products.Where(h => h.catid.ToString().Equals(id.ToString())).Select(h => h).ToList();
            }

            return View(Products);
        }
        public PartialViewResult _Nav()
        {
            var categori = db.categories.Select(n => n);
            return PartialView(categori);
        }
        [ChildActionOnly]
        public ActionResult _Aside()
        {
            var Products = db.products.OrderByDescending(h => h.stock).Select(h => h).Take(3);
            return PartialView(Products);
        }
        public ActionResult Chi_Tiet(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            product products = db.products.Find(id);
            if (products == null)
            {
                return HttpNotFound();
            }
            return View(products);
        }
    }
}