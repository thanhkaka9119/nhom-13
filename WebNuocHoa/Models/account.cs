namespace WebNuocHoa.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("account")]
    public partial class account
    {
        public int id { get; set; }

        [StringLength(100)]
        public string username { get; set; }

        [StringLength(100)]
        public string fullname { get; set; }

        public int? phone { get; set; }

        [StringLength(50)]
        public string passwords { get; set; }

        [StringLength(50)]
        public string email { get; set; }

        public bool? role { get; set; }
    }
}
