﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebNuocHoa.Models;

namespace WebNuocHoa.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        WebDB db = new WebDB();
        // GET: Admin/Login
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login()
        {
            string userName = Request["UserName"];
            string password = Request["Password"];
            var user = db.accounts.Where(u => u.username.Equals(userName) && u.passwords.Equals(password)).ToList();
            if (user.Count() > 0)
            {
                Session["userName"] = user.FirstOrDefault().username;
                return RedirectToAction("Index", "Admin/Home");
            }
            else
            {
                ViewBag.Error = "Đăng nhập không thành công";
            }
            return View();
        }
    }
}